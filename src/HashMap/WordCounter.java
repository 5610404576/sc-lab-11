package HashMap;

import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}
	
	public void count(){
		
		String[] words = this.message.split(" ");
		for (String str : words) {
			if (wordCount.containsKey(str)){
				wordCount.put(str, wordCount.get(str)+1);
			}
			else {
				wordCount.put(str, 1);
			}
		}
	}

	public int hasWord(String word){
		if (wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		else {
			return 0;
		}
	}
}
