package Exception2;

public class Main {

	public static void main(String[] args) {
		Refrigerator refri = new Refrigerator(6);
		try{
			refri.put("Kiwi");
			refri.put("Banana");
			refri.put("Milk");
			refri.put("Chocolate");
			refri.put("Fish");
			refri.put("Water");
			refri.put("Apple");
		}
		catch(FullException e){
			System.err.println("Error: " + e.getMessage());
		}
		System.out.println("Before takeOut: "+refri.toString());
		System.out.println("takeOut: "+refri.takeOut("Berry"));
		System.out.println("takeOut: "+refri.takeOut("Water"));
		System.out.println("After takeOut: "+refri.toString());
	}

}
